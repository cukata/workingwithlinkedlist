# WorkingWithLinkedList

Cílem projektu je vytvořit funkci ( a k tomu potřebné třídy  ) add, která je schopná sečíst dva polynomi.<br>
Tyto polynomy jsou reprezentované spojovými seznami.<br>
Každá prvek spojového seznamu má tři atributy<br>
next => ukazuje na další prvek v seznamu<br>
num => číslo<br>
pow => mocnina čísla<br>
Funkce add vrací nový spojový seznam, který bude obsahovat součet těchto polynomů, které jsou zadané jako parametry.<br>
Polynomy jsou v řetězci zapsané od nejměnší mocniny po největší.<br>
příklad <br>
<br>
![](Poly.png) 
