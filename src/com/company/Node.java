package com.company;

public class Node {
   public int pow;
   public int num;
   public Node next;

    public Node(int pow, int num, Node next) {
        this.pow = pow;
        this.num = num;
        this.next = next;
    }

    public Node(int pow, int num) {
        this.pow = pow;
        this.num = num;
        this.next = null;
    }

    @Override
    public String toString() {
        return "pow=" + pow + ", num=" + num;
    }
}
