package com.company;

public class LinkedList {
    public Node head;

    public LinkedList(Node head) {
        this.head = head;
    }

    public LinkedList() {
        this.head = null;
    }

    public void add ( Node head, int num, int pow ) {
        if( head == null ){
            this.head = new Node( pow, num);
        }else if( head.next == null ){
            Node newNode = new Node(pow, num);
            head.next = newNode;
        }else {
            add( head.next, num, pow );
        }
    }

    public void print( Node head ){
        if( head != null ){
            System.out.print(head.toString());
            if( head.next != null ){
                System.out.print(" + ");
            }else {
                System.out.println();
            }
            print(head.next);
        }
    }
}
