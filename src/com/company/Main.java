package com.company;

public class Main {

    public static void decide( LinkedList l1, LinkedList l2, LinkedList res ){
        if( l1.head.pow == l2.head.pow ){
            res.add( res.head, l1.head.num + l2.head.num, l1.head.pow );
            l1.head = l1.head.next;
            l2.head = l2.head.next;
        }else if( l1.head.pow < l2.head.pow ) {
            res.add( res.head, l1.head.num, l1.head.pow );
            l1.head = l1.head.next;
        }else {
            res.add(res.head, l2.head.num, l2.head.pow);
            l2.head = l2.head.next;
        }
    }

    public static LinkedList add( LinkedList l1, LinkedList l2 ){
        LinkedList res = new LinkedList();
        while( l1.head != null && l2.head != null ){
            decide( l1, l2, res );
        }
        return res;
    }

    public static void main(String[] args) {
	// write your code here
        LinkedList l1 = new LinkedList( new Node( 0, 3 ) );
        l1.add(l1.head, 2, 1);
        l1.add( l1.head, 9, 3 );
        l1.print(l1.head);

        LinkedList l2 = new LinkedList( new Node( 0, 7 ) );
        l2.add(l2.head, 4, 2);
        l2.add( l2.head, -1, 3 );
        l2.print(l2.head);

        LinkedList l3  = add(l1, l2);
        l3.print(l3.head);
    }
}
